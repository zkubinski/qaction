#include "mainwindow.h"
#include <QBoxLayout>
#include <QPushButton>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    this->resize(600, 500);

    menuFile = new QMenu(this);
    menuFile->setTitle(QString(tr("Plik")));

    menuEdit = new QMenu(this);
    menuEdit->setTitle(QString(tr("Edycja")));

    menuTools = new QMenu(this);
    menuTools->setTitle(QString(tr("Narzędzia")));

    action = new QAction(this);
    action->setText(QString(tr("Zamknij")));
    action->setStatusTip(QString(tr("Kończy działanie programu")));

    menuFile->addAction(action);

    menuBarr = new QMenuBar(this);

    menuBarr->addMenu(menuFile);
    menuBarr->addMenu(menuEdit);
    menuBarr->addMenu(menuTools);

    setMenuBar(menuBarr);

    statusBar = new QStatusBar(this);
    statusBar->showMessage(QString(tr("Program gotowy do pracy")));

    setStatusBar(statusBar);

    QObject::connect(action, &QAction::triggered, this, &QMainWindow::close);
}

MainWindow::~MainWindow()
{
}


<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL" sourcelanguage="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="10"/>
        <source>Plik</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="13"/>
        <source>Edycja</source>
        <translation>Edit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="16"/>
        <source>Narzędzia</source>
        <translation>Tools</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="19"/>
        <source>Zamknij</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="20"/>
        <source>Kończy działanie programu</source>
        <translation>End the program</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="33"/>
        <source>Program gotowy do pracy</source>
        <translation>The program is ready to work</translation>
    </message>
</context>
</TS>

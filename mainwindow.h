#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QAction>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    QMenu *menuFile, *menuEdit, *menuTools;
    QMenuBar *menuBarr;
    QStatusBar *statusBar;
    QAction *action;
};
#endif // MAINWINDOW_H

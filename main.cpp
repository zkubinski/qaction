#include "mainwindow.h"

#include <QApplication>
#include <QTranslator>
#include <QInputDialog>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTranslator translate;
    QStringList langsList;
    langsList << "Polski" << "Angielski";

    const QString lang = QInputDialog::getItem(NULL, "Language", "Wybierz język", langsList);

    if(lang == "Angielski"){
        translate.load(":/qaction_en_US.qm");
    }
    else if(lang == "Polski"){
        translate.load(":/qaction_pl_PL.qm");
    }
    if(lang != "Angielski"){
        a.installTranslator(&translate);
    }

    MainWindow w;
    w.show();
    return a.exec();
}

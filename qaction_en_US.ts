<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US" sourcelanguage="pl_PL">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="10"/>
        <source>File</source>
        <translation>Plik</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="13"/>
        <source>Edit</source>
        <translation>Edycja</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="16"/>
        <source>Tools</source>
        <translation>Narzędzia</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="19"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="20"/>
        <source>End the program</source>
        <translation>Kończy działanie programu</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="33"/>
        <source>The program is readyto work</source>
        <translation>Program gotowy do pracy</translation>
    </message>
</context>
</TS>
